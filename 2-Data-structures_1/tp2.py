""" TP2 where we code some reverse complement functions """
# Import random and time
import random
import time

def reverse_complement_naive(seq):
    """ Take an input sequence and return its revcomp """
    # Empty return sequence
    ret = ""
    # For each nucleotide in seq
    for nucl in seq:
        # If it is an A
        if nucl == "A":
            # add T at the beginning of ret
            ret = "T" + ret
        # If it is an T
        if nucl == "T":
            # add A at the beginning of ret
            ret = "A" + ret
        # If it is an C
        if nucl == "C":
            # add G at the beginning of ret
            ret = "G" + ret
        # If it is an G
        if nucl == "G":
            # add C at the beginning of ret
            ret = "C" + ret
    # Return ret
    return ret

def reverse_complement_naive2(seq):
    """ Take an input sequence and return its revcomp """
    # Empty return sequence
    ret = ""
    # For each nucleotide IN REVERSE SEQ
    for nucl in seq[::-1]:
        # If it is an A
        if nucl == "A":
            # add T
            ret += "T"
        # If it is an T
        if nucl == "T":
            # add A
            ret += "A"
        # If it is an C
        if nucl == "C":
            # add G
            ret += "G"
        # If it is an G
        if nucl == "G":
            # add C
            ret += "C"
    # Return ret
    return ret

def reverse_complement_less_naive(seq):
    """ Take an input sequence and return its revcomp """
    # Hash of correspondence
    rv_val = {"A": "T", "T": "A", "C": "G", "G": "C"}
    # Empty return sequence
    ret = ""
    # For each nucleotide
    for nucl in seq[::-1]:
        # Add its rv to ret
        ret += rv_val[nucl]
    # Return ret
    return ret

def generator_of_sequence(size):
    """ Generate a random ntd sequence of length 'size' """
    # Return sequence
    ret = ""
    # While we did not have a long enough sequence
    for _ in range(size):
        # Add a new random ntd
        ret += random.choice("ACGT")
    # Return the sequence
    return ret

def main():
    """ The main of TP2 that compare different revcomp functions """

    # Create a random sequence of specific size
    seq = generator_of_sequence(1000000)

    # Get the current time
    start = time.time()
    # Test the first naive revcomp function
    res_naive = reverse_complement_naive(seq)
    # Get the current time
    end = time.time()
    # Print the execution time (2 digits precision)
    print(f"Naive: {end - start:.2f}s")

    # Get the current time
    start = time.time()
    # Test the second naive revcomp function
    res_naive2 = reverse_complement_naive2(seq)
    # Get the current time
    end = time.time()
    # Print the execution time (2 digits precision)
    print(f"Naive2: {end - start:.2f}s")

    # Get the current time
    start = time.time()
    # Test the less naive revcomp function
    res_less_naive = reverse_complement_less_naive(seq)
    # Get the current time
    end = time.time()
    # Print the execution time (2 digits precision)
    print(f"Less naive: {end - start:.2f}s")

    # Ensure that all results are identical
    if res_naive == res_naive2 and res_naive == res_less_naive:
        # Print that everything is good
        print("Sequences are identical")
    else:
        # Print that there is a problem somewhere
        print("There is a bug!!")

# Launch the main
main()
# Exit without error
exit(0)
# Always put one extra return line
